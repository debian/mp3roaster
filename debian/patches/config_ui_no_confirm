From: Sébastien NOBILI
Origin: upstream
Last-Update: 2011-10-19
Forwarded: not-needed
Description: Allow user to request a noninteractive session
 This patch allows the user to request mp3record to use only the
 values it already knows about, running without any confirmations.
 .
 The patch was submitted to the upstream author by the user, who, not
 anticipating any new versions, submitted it to me for Debian inclusion.
Index: mp3roaster-0.3.0/mp3roaster
===================================================================
--- mp3roaster-0.3.0.orig/mp3roaster	2011-10-19 11:50:02.000000000 -0500
+++ mp3roaster-0.3.0/mp3roaster	2011-10-19 12:15:08.000000000 -0500
@@ -63,6 +63,7 @@
     'opt_check_questions|questions|Q',
     'opt_wav_normalize|normalize|n',
     'opt_wav_no_normalize|no-normalize|N',
+    'opt_ui_no_confirm|no-confirmation|y',
 
     'verbose|v',
     'help|h',
@@ -99,6 +100,7 @@
   -Q, --questions   Replace questionmarks with underscores.
   -n, --normalize   Normalize WAV files before burning.
   -N, --no-normalize Don\'t normalize WAV files (useful to override config)
+  -y, --no-confirm  Don\'t prompt for confirmation.
   -v, --verbose     Enable verbose output.
   -h, --help        Show this help screen.
   -V, --version     Show program version.
@@ -241,6 +243,7 @@
     my $config_check_questions;
     my $config_wav_normalize;
     my $config_wav_no_normalize;
+    my $config_ui_no_confirm;
 
     # This options MUST be defined in the configuration file
     # or MUST be passed to the program as a command line option
@@ -259,7 +262,8 @@
         "config_check_quotes"       => \$config_check_quotes,
         "config_check_questions"    => \$config_check_questions,
         "config_wav_normalize"      => \$config_wav_normalize,
-	"config_wav_no_normalize"   => \$config_wav_no_normalize
+        "config_ui_no_confirm"      => \$config_ui_no_confirm,
+        "config_wav_no_normalize"   => \$config_wav_no_normalize
     );
 
     open('RC', $in_config_file_location);   # open file
@@ -717,7 +721,7 @@
 #
 sub compare_file_atip {
 
-    my ($in_atip_time, $in_file_time) = @_;
+    my ($in_atip_time, $in_file_time, $no_confirm) = @_;
     my $key;
     my @fifo;
 
@@ -734,15 +738,17 @@
         print " * WARNING: The files won\'t fit on CD\/RW \(CD: $in_atip_min min / File: $in_file_min min\)\n";
     }
 
-    print "   Do you want to continue? ([Any Key/n]) ";
+    unless ($no_confirm) {
+        print "   Do you want to continue? ([Any Key/n]) ";
 
-    open(TTY,"</dev/tty");
-    ReadMode "raw";
-    $key = ReadKey 0, *TTY;
-    ReadMode "normal";
+        open(TTY,"</dev/tty");
+        ReadMode "raw";
+        $key = ReadKey 0, *TTY;
+        ReadMode "normal";
 
-    print "\n";
-    (print "   Well then, bye bye human -- See ya.\n") && (exit 0) if ($key eq ('n'||'N'));
+        print "\n";
+        (print "   Well then, bye bye human -- See ya.\n") && (exit 0) if ($key eq ('n'||'N'));
+    }
 }
 
 # }}}
@@ -802,18 +808,20 @@
 #
 sub check_for_wav_file {
 
-    my ($i, $file, $wavpath) = @_;
-    my $key;
+    my ($i, $file, $wavpath, $no_confirm) = @_;
+    my $key = 'y';
 
     if (-f $wavpath) {
 
-        print "\r                                                                                ";
-        print "\r   File $file already decoded, decode again? ([Any Key/n]) ";
+        unless ($no_confirm) {
+            print "\r                                                                                ";
+            print "\r   File $file already decoded, decode again? ([Any Key/n]) ";
 
-        open(TTY,"</dev/tty");
-        ReadMode "raw";
-        $key = ReadKey 0, *TTY;
-        ReadMode "normal";
+            open(TTY,"</dev/tty");
+            ReadMode "raw";
+            $key = ReadKey 0, *TTY;
+            ReadMode "normal";
+        }
 
         if ($key eq ('n'||'N')) {
 
@@ -844,7 +852,7 @@
 #
 sub file_decode {
 
-    my ($config_wav_normalize, $config_temp_dir, $config_mp3_decoder, @file_array) = @_;
+    my ($config_wav_normalize, $config_temp_dir, $config_mp3_decoder, $no_confirm, @file_array) = @_;
     my $sub = "FILE_DECODE";
     my $wavpath;
     my $tmpFileType;    # Stores the temporary file type from check_file_type().
@@ -853,7 +861,6 @@
     my $cmd;
     my $rc;
 
-
     foreach my $file (@file_array) {
 
         $wavpath = (basename $file);
@@ -896,7 +903,7 @@
             $wavpath =~ s/mp3/wav/i;
             $wavpath = $config_temp_dir."/".$wavpath;
 
-            if (!check_for_wav_file($i, $file, $wavpath)) {
+            if (!check_for_wav_file($i, $file, $wavpath, $no_confirm)) {
 
                 # unofficial Perl case
                 for ($config_mp3_decoder) {
@@ -945,7 +952,7 @@
             $wavpath =~ s/ogg/wav/i;
             $wavpath = $config_temp_dir."/".$wavpath;
 
-            if (!check_for_wav_file($i, $file, $wavpath)) {
+            if (!check_for_wav_file($i, $file, $wavpath, $no_confirm)) {
 
                 #
                 #   Function system returns the exit code, exec not.
@@ -982,7 +989,7 @@
             $wavpath =~ s/flac/wav/i;
             $wavpath = $config_temp_dir."/".$wavpath;
 
-            if (!check_for_wav_file($i, $file, $wavpath)) {
+            if (!check_for_wav_file($i, $file, $wavpath, $no_confirm)) {
 
                 $cmd = "flac -d  -s \"$file\" -o \"$wavpath\" 2>&-";
                 $rc = system($cmd);
@@ -1211,7 +1218,7 @@
             #
             # 5: check if files fit on CD/RW
             #
-            compare_file_atip(fetch_atip_time($confhash{config_cdr_dev}), fetch_file_time(@file_array));
+            compare_file_atip(fetch_atip_time($confhash{config_cdr_dev}), fetch_file_time(@file_array), $confhash{config_ui_no_confirm});
 
             #
             # 6: check the temp directory
@@ -1230,7 +1237,7 @@
             #
             print " * Decoding MP3/OGG/FLAC files to wav...\n";
 
-            @wav_array = file_decode($confhash{config_wav_normalize}, $confhash{config_temp_dir}, $confhash{config_mp3_decoder}, @file_array);
+            @wav_array = file_decode($confhash{config_wav_normalize}, $confhash{config_temp_dir}, $confhash{config_mp3_decoder}, $confhash{config_ui_no_confirm}, @file_array);
 
             #
             # TODO
@@ -1395,6 +1402,10 @@
 Don't normalize WAV files, even if --normalize is specified (either as
 a command line argument or in the configuratino file)
 
+=item B<-y, --no-confirm>
+
+Don't prompt for confirmation.
+
 =item B<-v, --verbose>
 
 Enable verbose output
