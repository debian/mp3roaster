#
# MP3Roaster INSTALL
#
# $Id: INSTALL,v 1.7 2005/10/09 14:04:40 eim Exp $
#

0. Index
--------

  0. Index
  1. Introduction
  2. Requirements
  3. Install MP3Roaster
  4. Uninstall MP3Roaster


1. Introduction
---------------

  In this file I will cover some basic instruction on how to install MP3Roaster
  on your system. Both installing and uninstalling the software should be
  quite easy and work on most UNIX like environments.

  MP3Roaster has been developed and tested on both Debian GNU/Linux and Red Hat
  (Fedora) Linux, all dependencies to run MP3Roaster are available for this
  GNU/Linux distributions.


2. Requirements
---------------

  Here is a list of software dependencies of MP3Roaster, in poor words you
  need the free software listed below in order to run MP3Roaster on your box.

    . Perl                  Perl 5.6 or above (tested with 5.8)
    . Term::ReadKey         Module for simple terminal control
    . File::MMagic          Perl library to Guess file type
    . MPEG::MP3Info         Perl library Manipulate / fetch info from MP3 audio files
    . Ogg::Vorbis::Header   Perl extension for Ogg Vorbis streams
    . Audio::FLAC::Header   Perl extension for FLAC files
    . Audio::Wav            Perl extension for WAV files

    . cdrecord              A command line CD writing tool
    . mpg321 or mpg123      A Free command-line mp3 player
    . ogg123                Plays Ogg Vorbis files
    . flac                  Decodes FLAC files

  Furthermore I suggest to install:

    . normalize             Adjust the volume of WAV files to a standard volume
                            level

  In order to install MP3Roaster from source and not as a Debian package
  you will also need this package:

    . GNU make              The GNU version of the "make" utility.

  Here is a table of required Perl libraries and it's relative package names
  for various GNU/Linux distributions, if you can provide further infos to
  complete this table please drop us a line, thank you.

    +---------------------+---------------------------+------------------------+-----------------+
    | Perl library        | Debian GNU/Linux          | Red Hat Linux          | Slackware Linux |
    +---------------------+---------------------------+------------------------+-----------------+
    | Term::ReadKey       | libterm-readkey-perl      | perl-TermReadKey       | Unknown         |
    | File::MMagic        | libfile-mmagic-perl       | perl-File-MMagic       | Unknown         |
    | MPEG::MP3Info       | libmp3-info-perl          | perl-MP3-Info          | Unknown         |
    | Ogg::Vorbis::Header | libogg-vorbis-header-perl | perl-Ogg-Vorbis-header | Not available   |
    | Audio::FLAC::Header | libaudio-flac-header-perl | perl-Audio-FLAC-header | Unknown         |
    | Audio::Wav          | libaudio-wav-perl         | perl-Audio-Wav         | Unknown         |
    +---------------------+---------------------------+------------------------+-----------------+

  Red Hat Linux packages note:

    All Red Hat packages listed in the above table are currently available from
    ftp://prince.homelinux.org/software

  Debian GNU/Linux packages note:

    All the above listed packages are available in Debian GNU/Linux unstable.


3. Install MP3Roaster
---------------------

  Once ensured that you have installed all the required software to run
  MP3Roaster and once download the latest source package or the CVS version
  of MP3Roaster you can proceed with this steps:

    1. Untar the MP3Roaster source package if downloaded as package:
       (If you have checked out the CVS source skip this step)

        % tar xfvz mp3roaster-<version>.tar.gz

    2. Move to the decompressed source folder and as root run:

        % cd mp3roaster-<version>       Go to the source
        % su -                          Become root with su
        % make install                  Launch installation

  That's it, now MP3Roaster is installed system wide on your box, this means
  that all users of your system can use it.

  Do not remove the MP3Roaster source directory because you will need it to
  eventually uninstall the software, see "Uninstall MP3Roaster" for details.


4. Uninstall MP3Roaster
-----------------------

  Uninstalling MP3Roaster is as easy as installing it.
  Here are described the two simple steps for uninstalling the software:

    % cd mp3roaster-<version>       Go to the source
    % su -                          Become root with su
    % make uninstall                Launch uninstall

  That's it, now MP3Roaster is completely removed from your computer.
  Now you can delete also the MP3Roaster source directory.
