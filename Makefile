#
# MP3Roaster Makefile
#
# $Id: Makefile,v 1.16 2005/12/28 21:45:40 eim Exp $
#
# This Makefile allows to install MP3Roaster system wide, this way MP3roaster
# can be used by all user of our system with personal configuration files in
# their respective home directories.
#

CFG_CVS_REVISION = \$$Revision: 1.16 $
BINPATH  = /usr/local/bin
MANPATH  = /usr/local/share/man
DOCPATH  = /usr/local/share/doc
ETCPATH  = /etc

all:
	@echo "MP3Roaster Makefile, $(CFG_CVS_REVISION)"
	@echo
	@echo "    . Type \"% make install\", to install MP3Roaster."
	@echo "    . Type \"% make uninstall\", to uninstall MP3Roaster."
	@echo
	@echo "This Makefile will install MP3Roaster system wide."
	@echo "Read the MP3roaster man page or perldoc \`which mp3roaster\` once installed."
	@echo

install:
	@echo
	@echo "INSTALLING MP3Roaster"
	@echo "====================="
	@echo
	@echo "This procedure will install MP3Roaster."
	@echo

# EXECUTABLES
	@echo -n -e "\t* Installing binaries .............. "
	@install -D mp3roaster $(BINPATH)/mp3roaster
	@echo "done."

# MAN PAGES
	@echo -n -e "\t* Installing manual ................ "
	@install -D -m 0644 mp3roaster.1.gz $(MANPATH)/man1/mp3roaster.1.gz
	@echo "done."

# DOCUMENTS
	@echo -n -e "\t* Installing documentation ......... "
	@install -d $(DOCPATH)/mp3roaster
	@install -m 0644 COPYING $(DOCPATH)/mp3roaster
	@install -m 0644 ChangeLog $(DOCPATH)/mp3roaster
	@install -m 0644 README $(DOCPATH)/mp3roaster
	@echo "done."

# /ETC CONFIGURATIONS
	@echo -n -e "\t* Installing configuration ......... "
	@install -d $(ETCPATH)/mp3roaster
	@install -m 0644 mp3roasterrc $(ETCPATH)/mp3roaster
	@echo "done."

	@echo
	@echo "Installed successfully."
	@echo

uninstall: 
	@echo
	@echo "UNINSTALLING MP3Roaster"
	@echo "======================="

	@echo
	@echo "This procedure will completely remove MP3Roaster from your system."
	@echo "Only MP3Roaster user configurations in the home directories will remain."
	@echo

# EXECUTABLES
	@echo -n -e "\t* Removing binaries .............. "
	@rm $(BINPATH)/mp3roaster
	@echo "done."

# MAN PAGES
	@echo -n -e "\t* Removing manual ................ "
	@rm $(MANPATH)/man1/mp3roaster.1.gz
	@echo "done."

# DOCUMENTS
	@echo -n -e "\t* Removing documentation ......... "
	@rm -r $(DOCPATH)/mp3roaster
	@echo "done."

# /ETC CONFIGURATIONS
	@echo -n -e "\t* Removing configuration ......... "
	@rm -r $(ETCPATH)/mp3roaster
	@echo "done."

	@echo
	@echo "Uninstalled successfully."
	@echo

changelog:
	echo -e "#\n# \$$Id: Makefile,v 1.16 2005/12/28 21:45:40 eim Exp $$\n#\n" | cvs2cl -T -t -w -r --fsf -U Developers --header - && rm ChangeLog.bak
	cvs commit -m "The ChangeLog has been updated." ChangeLog

cvs_update:
	cvs update -P

man:
	rm -f mp3roaster.1 mp3roaster.1.gz
	pod2man mp3roaster mp3roaster.1
	gzip mp3roaster.1
	cvs commit -m "The MAN page has been updated." mp3roaster.1.gz
